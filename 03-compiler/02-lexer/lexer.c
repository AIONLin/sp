#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define TMAX 10000000
#define SMAX 100000

enum { Id, Int, Keyword, Literal, Char };  // 列舉 將型態編號 Id就是1 Int是2 以此類推

char *typeName[5] = {"Id", "Int", "Keyword", "Literal", "Char"};

char code[TMAX]; // 程式碼  (和字串表很接近但只有最後有/0)
char strTable[TMAX], *strTableEnd=strTable;   // 字串表 字串表每個辭彙都有/0結尾
char *tokens[TMAX]; // 最多到TMAX的陣列 裡面每個元素都是字元指標
int tokenTop=0;
int types[TMAX];   // 儲存型態編號 

#define isDigit(ch) ((ch) >= '0' && (ch) <='9')

#define isAlpha(ch) (((ch) >= 'a' && (ch) <='z') || ((ch) >= 'A' && (ch) <= 'Z'))

int readText(char *fileName, char *text, int size) {
  FILE *file = fopen(fileName, "r");
  int len = fread(text, 1, size, file);
  text[len] = '\0';
  fclose(file);
  return len;
}

/* strTable =
#\0include\0"sum.h"\0int\0main\0.....
*/
char *next(char *p) {   // 執行一次就記住一個token
  while (isspace(*p)) p++;  // 略過空白

  
  char *start = p; //         include "sum.h"   //略過空白後真正詞彙開頭 把開頭記住放到start裡面 
                   //         ^      ^
                   //  start= p      p        
  int type;
  if (*p == '\0') return NULL;   // 情況1:略過空白後直接結尾
  if (*p == '"') {               // 情況2:取到"的話 要取到下一個才算結束
    p++;
    while (*p != '"') p++;    //*p取內容(以char*而言指的是字元) 只有p是代表指標p++指標++       //p++ 位址裡內容+1
    p++;
    type = Literal;
  } else if (*p >='0' && *p <='9') { // 情況3:數字
    while (*p >='0' && *p <='9') p++;  // 一直取0到9的 直到非0到9
    type = Int; // 目前只支援整數
  } else if (isAlpha(*p) || *p == '_') { // 情況4:變數名稱或關鍵字 英文或底線開頭(不能數字當第一個)
    while (isAlpha(*p) || isDigit(*p) || *p == '_') p++;  // 接下來可以繼續取英文或數字或底線
    type = Id;
  } else { // 情況5:單一字元  (非上面4種情況)
    p++;
    type = Char;
  }
  int len = p-start;   // 長度為p-start 因為p到token結尾
  char *token = strTableEnd;   // strTable   
  strncpy(strTableEnd, start, len);   //每次取到辭彙就放到字串表尾端(從start取len長度放進)
  strTableEnd[len] = '\0';    // 最後都會補0
  strTableEnd += (len+1);      // 往下繼續
  types[tokenTop] = type;        // 記住型態
  tokens[tokenTop++] = token;
  printf("token=%s\n", token);
  return p;
}

void lex(char *fileName) { // 最重要的函數
  char *p = code;  //  先把指標設成code
  while (1) {
    p = next(p); //一個一個取出
    if (p == NULL) break;  //取到最後傳回一個null 並結束
  }
}

void dump(char *strTable[], int top) {  // strTable[]是一個陣列 內容為字元指標(char *) --> 字元指標陣列
  for (int i=0; i<top; i++) {
    printf("%d:%s\n", i, strTable[i]);
  }
}

int main(int argc, char * argv[]) {
  readText(argv[1], code, sizeof(code)); //把程式讀進來放到code這個字串裡 讀sizeof(code)大小(這裡的大小不會超過第12行定義的TMAX)
                          // sizeof(code) --> sizeof(code)-1 比較好 因為還有結尾0 讀取時會超過
  puts(code);  // 讀進來後 印出來
  lex(code);   // 這程式的重點 解析詞彙
  dump(tokens, tokenTop);  // 將詞彙放在tokens陣列裡面  tokenTop放解析出來的數量
}
 
