#define STDIN 0
#define STDOUT 1
#define STDERR 2
#define CONSOLE 1

struct file {
  enum { FD_NONE, FD_PIPE, FD_INODE, FD_DEVICE } type;
  int ref; // reference count
  char readable;
  char writable;
  struct pipe *pipe; // FD_PIPE
  struct inode *ip;  // FD_INODE and FD_DEVICE
  uint off;          // FD_INODE
  short major;       // FD_DEVICE
};

#define NFILE 128
#define NDEV  32

struct ftable {
  // struct spinlock lock;
  struct file file[NFILE];
};

// map major device number to device functions.
struct devsw {
  ssize_t (*read)(int, void *, size_t);
  ssize_t (*write)(int, const void *, size_t);
};

extern struct devsw devsw[NDEV];

ssize_t read(int fd, void *buf, size_t count);
ssize_t write(int fd, const void *buf, size_t count);
