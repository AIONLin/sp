struct ftable ftable;
struct devsw devsw[NDEV];

ssize_t cdev_read(struct file *f, void *buf, size_t count, int (*_getc)(void)) {
  char *cbuf = buf;
  for(int i = 0; i < n; i++){
    int ch = _getc();
    cbuf[i] = ch;
    if (ch == EOF) break;
  }
  return i;
}

ssize_t cdev_write(struct file *f, const void *buf, size_t count, int (*_putc)(void)) {
  char *cbuf = buf;
  for(int i = 0; i < n; i++){
    int ch = _putc();
    cbuf[i] = ch;
    if (ch == EOF) break;
  }
  return i;
}

ssize_t console_read(const void *buf, size_t count) {
  return cdev_read(&ftable.file[1], buf, count, uart_getc);
}

ssize_t console_write(const void *buf, size_t count) {
  return cdev_write(&ftable.file[1], buf, count, uart_putc);
}

void nix_init(void) {
  devsw[CONSOLE].read = console_read;
  devsw[CONSOLE].write = console_write;
}
