// Allocate a file structure.
struct file* fileopen(void) {
  struct file *f;

  for(f = ftable.file; f < ftable.file + NFILE; f++){
    if(f->ref == 0){
      f->ref = 1;
      return f;
    }
  }
  return 0;
}

// Close file f.  (Decrement ref count, close when reaches 0.)
void fileclose(struct file *f) {
  struct file ff;

  ff = *f;
  f->ref = 0;
  f->type = FD_NONE;
  // iput(ff.ip);
}

// addr is a user virtual address.
int fileread(struct file *f, uint64 addr, int n) {
  int r = 0;

  if(f->readable == 0)
    return -1;

  r = devsw[f->major].read(1, addr, n);
  return r;
}
// bp = bread(ip->dev, IBLOCK(ip->inum, sb));
// Write to file f.
// addr is a user virtual address.
int filewrite(struct file *f, uint64 addr, int n) {
  int r, ret = 0;

  if(f->writable == 0)
    return -1;

  devsw[f->major].write(1, addr, n);
  return ret;
}
